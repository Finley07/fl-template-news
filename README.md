## 新闻资讯列表详情页面模板
***
### 使用说明：
将pages文件夹下index、details文件复制至自己的项目内即可。<br />
详情页内使用了官方icon插件，请注意一同下载：[uni-icons 图标 - DCloud 插件市场](https://ext.dcloud.net.cn/plugin?id=28)

### 列表页
#### newsData

| 字段名 | 类型 | 说明 |
|--|--|--|
| id | Number |  |
| title | String | 文章标题 |
| medium | String | 媒体名称 |
| time | String | 发布时间 |
| image | Array | 图片 |

#### methods

| 函数名 | 说明 |
|--|--|
| getNewsData | 获取列表数据 |
| toDetails | 跳转至详情页 |

#### 页面布局

列表页共四种布局样式：标题（无图）、单图、双图、三图。这里会根据image数组长度判断，无需其他逻辑接入。<br />
也就是说，无论有无图片或是几张图片，image都应是一个数组。
***

### 详情页
#### details

| 字段名 | 类型 | 说明 |
|--|--|--|
| id | Number |  |
| title | String | 文章标题 |
| medium | String | 媒体名称 |
| time | String | 发布时间 |
| pageviewNum | Number | 浏览量 |
| html | String | 文章的富文本内容 |
| likeNum | Number | 点赞数 |
| collectNum | Number | 收藏数 |
| commentNum | Number | 评论数 |
| isLike | Boolean | 当前用户是否点赞 |
| isCollect | Boolean | 当前用户是否收藏 |
| commentData | Array | 评论列表 |

#### commentData

| 字段名 | 类型 | 说明 |
|--|--|--|
| avatar | String | 头像 |
| nickname | String | 昵称 |
| content | String | 评论内容 |
| location | String | ip定位 |
| time | String | 发布时间 |

#### methods

| 函数名 | 说明 |
|--|--|
| getNewsDetails | 获取详情数据 |
| like | 点赞\取消点赞 |
| collect | 收藏\取消收藏 |
| share | 分享 |
| sendComment | 发送评论 |
| scrollToTop | 滚动页面至顶部 |
| scrollToComment | 滚动页面至评论位置 |


### 页面示例预览
![fl-template-news.png](https://raw.gitcode.com/Finley07/fl-template-news/attachment/uploads/8955811e-8893-472d-8c66-6bbf314f67fd/fl-template-news.png 'fl-template-news.png')

[http://template.finley.xin/fl-template-news](http://template.finley.xin/fl-template-news)

### 其他
关于广告：开源不易，生活不易，广告下载，实属无奈，理解万岁。<br />
我将陆续推出其他项目及页面模板，旨在方便大家的开发工作。

End...

Good luck for us

© Created by Finley


